import {
	LOGIN,
	REGISTER,
	LOGIN_PAGE_UNLOADED,
	REGISTER_PAGE_UNLOADED,
	ASYNC_START,
	SET_INPUT
} from '../constants/actionTypes';

const initialState = {
	errorSocial: false
};

export default (state = initialState, action) => {
	switch (action.type) {
		case LOGIN:
		case REGISTER:
			return {
				...state,
				inProgress: false,
				errorSocial: action.error ? true : false,
				errors: action.payload ? action.payload.errors : false,
				loading: true
			};
		case LOGIN_PAGE_UNLOADED:
		case REGISTER_PAGE_UNLOADED:
			return {};
		case ASYNC_START:
			if (action.subtype === LOGIN || action.subtype === REGISTER) {
				return { ...state, inProgress: true };
			}
			break;
		case SET_INPUT:
			return { ...state, [action.key]: action.value };
		default:
			return state;
	}

	return state;
};
