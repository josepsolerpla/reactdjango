import {
    UPDATE_FIELD_CONTACT,
    CONTACT
  } from '../constants/actionTypes';
const initialState = {
  errormessage : '', erroremail : '' , errorname : '' , email : '' , name : '' , message : '' , errors : '' 
}
export default (state = initialState, action) => {
  
  switch (action.type) {
    case UPDATE_FIELD_CONTACT:
      let regExp = /^/;
      let message = null
      switch (action.event.target.name) {
        case "email":
            regExp = /^([a-zA-Z0-9_\-\]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/;
            message = "The Email is wrong"
          break;
        case "name":
            regExp = /^.{6,}$/
            message = "the Name must be 6 characters"
          break;
        case "message":
            regExp = /^.{20,}$/
            message = "The Message must be 20 characters"
          break;
        default:
          break;
      }
      if(!regExp.test(action.event.target.value)){
        return { ...state, [action.event.target.name]: action.event.target.value, ["error"+action.event.target.name] : message};
      }else{
        return { ...state, [action.event.target.name]: action.event.target.value, ["error"+action.event.target.name] : null };
      }
    case CONTACT:
      return {
        ...state,
        inProgress: false,
        errors: action.error ? action.payload.errors : null
      };
    default :
      return state;
  }
};