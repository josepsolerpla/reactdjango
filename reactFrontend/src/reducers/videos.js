import { VIDEOS, CHANGE_VALUE, VIDEO } from '../constants/actionTypes';

this.state = {
	videos: [],
	videosCount: 0,
	currentPage: 1,
	searchValue: '',
	errorServer: false
};

export default (state = this.state, action) => {
	switch (action.type) {
		case VIDEOS:
			if (action.payload) {
				return {
					...state,
					videos: action.payload.videos,
					videosCount: action.payload.videosCount,
					currentPage: action.page,
					errorServer: false
				};
			} else {
				return {
					...state,
					videos: [],
					videosCount: 0,
					errorServer: true,
					currentPage: 0
				};
			}
		case VIDEO:
			if (action.error) return { ...state, error: true };
			return {
				...state,
				video: action.payload.videos
			};
		case CHANGE_VALUE:
			state[action.target] = action.value;
			return {
				...state
			};
		default:
			return state;
	}
};
