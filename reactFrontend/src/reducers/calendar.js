import {
  CALENDARS,
  CALENDARS_VIEW
  } from '../constants/actionTypes';

this.state = {
  calendars: [],
  calendarsCount: 0,
  currentPage : 1
}

  export default (state = this.state, action) => {
    switch (action.type) {
      case CALENDARS:
      return {
          ...state,
          calendars: action.payload.calendars,
          calendarsCount: action.payload.calendarsCount,
          currentPage : action.page
        };
      case CALENDARS_VIEW:
      return {
        ...state,
        calendar: action.payload.calendar,
      };      
      default:
        return state;
    }
  };
  