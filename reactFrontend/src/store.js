import { applyMiddleware, createStore } from 'redux';
// import { createLogger } from 'redux-logger'
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { promiseMiddleware, localStorageMiddleware } from './middleware';
import reducer from './reducer';

import { routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';

export const history = createHistory();

// Build the middleware for intercepting and dispatching navigation actions
const myRouterMiddleware = routerMiddleware(history);

const getMiddleware = () => {
	if (process.env.NODE_ENV === 'production') {
		return applyMiddleware(myRouterMiddleware, promiseMiddleware, localStorageMiddleware);
	} else {
		// Enable additional createLogger in non-production environments or just becouse is disggusting
		// return applyMiddleware(myRouterMiddleware, promiseMiddleware, localStorageMiddleware, createLogger())

		return applyMiddleware(myRouterMiddleware, promiseMiddleware, localStorageMiddleware);
	}
};

export const store = createStore(
	reducer,
	// Take the store of localStorage and parse it to use it as a initial state
	typeof Storage && window.localStorage.getItem('redux-store')
		? JSON.parse(window.localStorage.getItem('redux-store'))
		: {},
	composeWithDevTools(getMiddleware())
);

/**
 * This wil store all the reducers into the localstorage sistem, will convert them into a string and then they will be parsed and inserted
 */
store.subscribe(() => {
	// Check browser support
	if (typeof Storage !== 'undefined') {
		window.localStorage.setItem('redux-store', JSON.stringify(store.getState()));
	}
});
