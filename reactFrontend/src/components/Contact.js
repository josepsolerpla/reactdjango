import React from 'react';
import { connect } from 'react-redux';
import agent from '../agent';
import ListErrors from './ListErrors';

import {
    UPDATE_FIELD_CONTACT,
    CONTACT
} from '../constants/actionTypes';
  

const mapStateToProps = state => ({ ...state.contact });

const mapDispatchToProps = dispatch => ({
    onChangeValue : (event,validation = /^/) => 
        dispatch({ type: UPDATE_FIELD_CONTACT, event }),
    onSubmit : form =>
        dispatch({ type: CONTACT, payload: agent.Contact.sendForm(form) }),
});

class Contact extends React.Component {
  constructor() {
        super();
        this.onChangeValue = () => ev => this.props.onChangeValue(ev);

        this.submitForm = () => ev => {
            ev.preventDefault();
            let form = {
                message : this.props.message,
                name : this.props.name,
                email : this.props.email
            }

            if(!this.props.erroremail && !this.props.errormessage && !this.props.errorname)
                this.props.onSubmit(form);
          };
    }

  render() {
    const { errormessage , erroremail , errorname , email , name , message , errors } = this.props;

    return (
        <form onSubmit={this.submitForm()}>
            <ListErrors errors={errors} />
            <fieldset>
                Email:
                <input type="text" placeholder="Input your Email Address" name="email" value={email} onChange={this.onChangeValue()} />
                <span name="errorEmail">{erroremail}</span>
            </fieldset><br></br>
            <fieldset>
                Name:
                <input type="text" placeholder="Input your Name" name="name" value={name} onChange={this.onChangeValue()} />
                <span name="errorName">{errorname}</span>
            </fieldset><br></br>
            <fieldset>
                Message:
                <textarea value={message} name="message" onChange={this.onChangeValue()} />
                <span name="errorMessage">{errormessage}</span>
            </fieldset><br></br>

            <input type="submit" value="Submit" />
        </form>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Contact);