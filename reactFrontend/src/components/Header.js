import React from 'react';
import { Link } from 'react-router-dom';

const LoggedOutView = (props) => {
	if (!props.currentUser) {
		return (
			<ul className="nav navbar-nav pull-xs-right">
				<li className="nav-item">
					<Link to="/videos" className="nav-link">
						<i className="fas fa-list-ul" />
						<span> Videos</span>
					</Link>
				</li>

				<li className="nav-item">
					<Link to="/contact" className="nav-link">
						Contact
					</Link>
				</li>

				<li className="nav-item">
					<Link to="/login" className="nav-link">
						Sign in
					</Link>
				</li>

				<li className="nav-item">
					<Link to="/register" className="nav-link">
						Sign up
					</Link>
				</li>
			</ul>
		);
	}
	return null;
};

const LoggedInView = (props) => {
	if (props.currentUser) {
		return (
			<ul className="nav navbar-nav pull-xs-right">
				<li className="nav-item">
					<Link to="/videos" className="nav-link">
						<i className="fas fa-list-ul" />
						<span> Videos</span>
					</Link>
				</li>

				<li className="nav-item">
					<Link to={`/${props.currentUser.username}/upload`} className="nav-link">
						<i className="fas fa-upload" />
						<span> Upload</span>
					</Link>
				</li>

				<li className="nav-item">
					<Link
						to={`/@${props.currentUser.username ? props.currentUser.username : null}`}
						className="nav-link"
					>
						<img
							src={props.currentUser.image ? props.currentUser.image : 'http://i.pravatar.cc/300'}
							className="user-pic"
							alt={props.currentUser.username}
						/>
						{props.currentUser.username}
					</Link>
				</li>
			</ul>
		);
	}

	return null;
};

class Header extends React.Component {
	render() {
		return (
			<nav className="navbar navbar-light">
				<div className="container">
					<Link to="/" className="navbar-brand">
						{this.props.appName.toLowerCase()}
					</Link>

					<LoggedOutView currentUser={this.props.currentUser} />

					<LoggedInView currentUser={this.props.currentUser} />
				</div>
			</nav>
		);
	}
}

export default Header;
