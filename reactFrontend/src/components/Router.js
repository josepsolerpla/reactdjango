import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import Article from './Article';
import Editor from './Editor';
import Home from './Home';
import Contact from './Contact';
import Login from './Login';
import VideosSearcher from './Videos/VideosSearcher';
import VideosPreview from './Videos/VideosPreview';
import UploadVideos from './Videos/UploadVideos';
import Profile from './Profile';
import ProfileFavorites from './ProfileFavorites';
import Register from './Register';
import Settings from './Settings';

/**
 * Router Component 
 * 
 * Componetizate this Router will make your application more efficient at the moment to incorporate more microapps
 */
class Router extends React.Component {
	render() {
		const { props } = this.props;

		return (
			<div>
				<Switch>
					<Route exact path="/" component={Home} isAuth={true} />
					<Route path="/contact" component={Contact} />
					<Route path="/login" component={Login} />
					<Route path="/register" component={Register} />
					<Route path="/editor/:slug" component={Editor} />
					<Route path="/editor" component={Editor} />
					<Route path="/article/:id" component={Article} />
					<Route path="/settings" component={Settings} />
					<Route path="/@:username/favorites" component={ProfileFavorites} />
					<Route path="/@:username" component={Profile} />
					<Route path="/videos/:slug" component={VideosPreview} />
					<Route path="/videos" component={VideosSearcher} />
					{props.currentUser ? (
						<Route path={`/${props.currentUser.username}/upload`} component={UploadVideos} />
					) : null}
				</Switch>
			</div>
		);
	}
}

export default connect()(Router);
