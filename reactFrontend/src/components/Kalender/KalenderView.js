import React from 'react'
import { connect } from 'react-redux';
import agent from '../../agent';

import KalenderListing from './KalenderListing'

import {
    CALENDARS
} from '../../constants/actionTypes';

const mapStateToProps = state => ({ ...state.calendar });

const mapDispatchToProps = dispatch => ({
    onLoad : (page = 0) =>
        dispatch({ type: CALENDARS, payload: agent.Calendars.getAll(page),page }),
});

class KalenderView extends React.Component {
    componentWillMount(){
        this.props.onLoad();
    }

    changePage = (e, { activePage }) => this.props.onLoad( activePage - 1 )

    render(){
        const { calendars , calendarsCount , currentPage} = this.props
        return (
            <KalenderListing calendars={calendars} calendarsCount={calendarsCount} changePage={this.changePage} currentPage={currentPage+1}/>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(KalenderView);