import React from 'react';
import { Pagination } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

class KalenderListing extends React.Component {
	render() {
		const { calendars, calendarsCount, currentPage, changePage } = this.props;
		const list = calendars ? (
			calendars.map((calendar) => {
				// return (<p>{calendar.name}</p>)
				return (
					<li key={calendar.name}>
						<Link to={'/calendarView/' + calendar.slug} className="nav-link">
							{calendar.name}
						</Link>
					</li>
				);
			})
		) : (
			<p>Loading</p>
		);

		return (
			<div>
				{list}
				<Pagination
					// defaultActivePage={calendarsCount}
					totalPages={calendarsCount / 5}
					activePage={currentPage}
					onPageChange={changePage}
				/>
			</div>
		);
	}
}

export default KalenderListing;
