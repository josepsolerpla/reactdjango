import React from 'react'
import { connect } from 'react-redux';
import agent from '../../agent';

import {
    CALENDARS_VIEW
} from '../../constants/actionTypes';

const mapStateToProps = state => ({ 
    ...state.calendar
 });

const mapDispatchToProps = dispatch => ({
    onLoad : slug =>
        dispatch({ type: CALENDARS_VIEW, payload: agent.Calendars.getCalendar(slug) }),
});

class KalenderPreview extends React.Component {
    componentWillMount(){
        this.props.onLoad(this.props.match.params.slug);
    }

    changePage = (e, { activePage }) => this.props.onLoad( activePage - 1 )

    render(){
        const { calendar} = this.props
        return (
                <div>
                    {calendar ? calendar.name : 'loading'}
                </div>
            )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(KalenderPreview);