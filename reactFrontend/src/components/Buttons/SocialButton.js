import React, { Component } from 'react';
import { connect } from 'react-redux';
import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login';
import TwitterLogin from 'react-twitter-auth';
import agent from '../../agent';
import { LOGIN } from '../../constants/actionTypes';

const mapDispatchToProps = (dispatch) => ({
	onSubmit: (username, email, password) =>
		dispatch({ type: LOGIN, payload: agent.Auth.socialLogin(username, email, password) })
});
const mapStateToProps = (state) => ({});

class SocialButton extends Component {
	constructor() {
		super();
		this.onSubmit = this.onSubmit.bind(this);
	}
	onSubmit(form) {
		console.log(form.profileObj.googleId);
		this.props.onSubmit(form.profileObj.name, form.profileObj.email, form.profileObj.googleId);
	}

	render() {
		const { button } = this.props;
		switch (button) {
			case 'SocialLoginGoogle':
				return <SocialLoginGoogle response={this.onSubmit} />;
			case 'SocialLoginFacebook':
				return <SocialLoginFacebook response={this.onSubmit} />;
			case 'SocialLoginTwitter':
				return <SocialLoginTwitter response={this.onSubmit} />;

			default:
				return <SocialLoginGoogle response={this.onSubmit} />;
		}
	}
}

const SocialLoginGoogle = (props) => {
	return (
		<GoogleLogin
			clientId="498131447313-4to6a3hs5d1s51ijlf16g2s8893puuln.apps.googleusercontent.com"
			buttonText="Login"
			onSuccess={props.response}
			onFailure={props.response}
		/>
	);
};

const SocialLoginFacebook = (props) => {
	return (
		<FacebookLogin
			appId="164176167742496"
			autoLoad={true}
			fields="name,email,picture"
			onSuccess={props.response}
			onFailure={props.response}
			icon="fa-facebook"
			textButton="Login"
			cssClass="my-facebook-button-class"
		/>
	);
};

const SocialLoginTwitter = (props) => {
	return (
		<TwitterLogin
			loginUrl="http://localhost:4000/api/v1/auth/twitter"
			onFailure={props.response}
			onSuccess={props.response}
			requestTokenUrl="http://localhost:4000/api/v1/auth/twitter/reverse"
			showIcon={true}
			text="Login"
			className="my-twitter-button-class"
		/>
	);
};

export default connect(mapStateToProps, mapDispatchToProps)(SocialButton);
