import React from 'react';
import VideosListing from '../Videos/VideosListing';
import agent from '../../agent';
import { connect } from 'react-redux';
import { VIDEOS, CHANGE_VALUE } from '../../constants/actionTypes';

const mapStateToProps = (state) => ({
	...state.videos,
	currentUser: state.common.currentUser
});

const mapDispatchToProps = (dispatch) => ({
	onLoad: () => dispatch({ type: VIDEOS, payload: agent.Videos.getMyVideos() }),
	changeValue: (target, value) => {
		dispatch({ type: CHANGE_VALUE, target, value });
		dispatch({
			type: VIDEOS,
			payload: agent.Videos.getMyVideos(value),
			page: 0
		});
	}
});

class MainView extends React.Component {
	constructor(props) {
		super(props);
		this.props.onLoad();
	}

	render() {
		const { videos, currentUser, changeValue } = this.props;
		console.log(videos);
		if (!currentUser || !videos)
			return (
				<div>
					<h1>Welcome to Kiddeos Login to see your videos</h1>
				</div>
			);

		return (
			<div className="col-md-9">
				<VideosListing
					changeValue={changeValue}
					videos={videos}
					title={'Your Videos'}
					paginationNotShow={true}
				/>
			</div>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(MainView);
