import { Link } from 'react-router-dom';
import ListErrors from './ListErrors';
import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import { SET_INPUT, LOGIN, LOGIN_PAGE_UNLOADED } from '../constants/actionTypes';
import SocialButton from './Buttons/SocialButton';

const mapStateToProps = (state) => ({ ...state.auth });

const mapDispatchToProps = (dispatch) => ({
	setInput: (target, value) => dispatch({ type: SET_INPUT, target, value }),
	onSubmit: (email, password) => dispatch({ type: LOGIN, payload: agent.Auth.login(email, password) }),
	onUnload: () => dispatch({ type: LOGIN_PAGE_UNLOADED })
});

class Login extends React.Component {
	constructor(props) {
		super(props);
		this.setInput = this.setInput.bind(this);
		this.login = this.login.bind(this);
		this.props.onUnload();
		this.state = {
			email: '',
			password: ''
		};
	}

	setInput(ev) {
		this.setState({ [ev.target.name]: ev.target.value });
	}

	login() {
		console.log(this.state);
		this.props.onSubmit(this.state.email, this.state.password);
	}

	render() {
		const { errorSocial } = this.props;
		const { email, password } = this.state;

		return (
			<div className="auth-page">
				<div className="container page">
					<div className="row">
						<div className="col-md-6 offset-md-3 col-xs-12">
							<h1 className="text-xs-center">Sign In</h1>
							<p className="text-xs-center">
								<Link to="/register">Need an account?</Link>
							</p>

							<ListErrors errors={this.props.errors} />

							<input
								className="form-control form-control-lg"
								type="email"
								placeholder="Email"
								value={email}
								name="email"
								onChange={this.setInput}
							/>
							<input
								className="form-control form-control-lg"
								type="password"
								placeholder="Password"
								value={password}
								name="password"
								onChange={this.setInput}
							/>
							<SocialButton button={'SocialLoginGoogle'} />
							<button
								className="btn btn-lg btn-primary pull-xs-right"
								type="submit"
								onClick={(ev) => (ev.preventDefault(), this.login())}
							>
								Sign in
							</button>
							{errorSocial ? <h1>Error on login</h1> : null}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
