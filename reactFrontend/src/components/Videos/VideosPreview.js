import React, { Component } from 'react';
import { connect } from 'react-redux';
import agent from '../../agent';
import { Player } from 'video-react';
import { VIDEO } from '../../constants/actionTypes';

class VideosPreview extends Component {
	constructor(props) {
		super(props);
		this.props.onLoad(this.props.match.params.slug);
	}
	render() {
		const { video, currentUser } = this.props;
		console.log(this.props);
		if (!video) return <img src="https://www.voya.ie/Interface/Icons/LoadingBasketContents.gif" alt="loading" />;
		return (
			<div>
				<Player
					playsInline
					poster={video.thumbnail}
					src={video.video}
					fluid={false}
					width={'100%'}
					height={720}
				/>
				<aside className="descriptionView">
					<section>
						<img
							src={video.author.image ? video.author.image : 'http://i.pravatar.cc/300'}
							alt="author_image"
						/>
						<h1>{video.author.username}</h1>
						<button
							disabled={currentUser && currentUser.username === video.author.username ? true : false}
							style={{
								backgroundColor:
									currentUser && currentUser.username === video.author.username ? '#444' : false
							}}
						>
							Subscribe
						</button>
						<button>{'<3'}</button>
					</section>
					<section>
						<p>{video.description}</p>
					</section>
				</aside>
			</div>
		);
	}
}
const mapStateToProps = (state) => ({
	...state.videos,
	currentUser: state.common.currentUser
});

const mapDispatchToProps = (dispatch) => ({
	onLoad: (slug) => dispatch({ type: VIDEO, payload: agent.Videos.get(slug) })
});
export default connect(mapStateToProps, mapDispatchToProps)(VideosPreview);
