import { connect } from 'react-redux';
import React from 'react';
import { Component } from 'react';
import { CHANGE_VALUE, VIDEO } from '../../constants/actionTypes';
import { WithContext as ReactTags } from 'react-tag-input';
import agent from '../../agent';

class UploadVideos extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loadFile: false,
			loadThumbnail: false,
			tags: [ { id: 'video', text: 'video' } ],
			suggestions: [ { id: 'MEGAVIDEO', text: 'MEGAVIDEO' }, { id: 'RICO', text: 'RICO' } ]
		};
		this.changeState = this.changeState.bind(this);
		this.handleDelete = this.handleDelete.bind(this);
		this.handleAddition = this.handleAddition.bind(this);
		this.uploadVideo = this.uploadVideo.bind(this);
		this.handleDrag = this.handleDrag.bind(this);
	}

	// Functions used on the ` react-tag-input ` component
	handleDelete(i) {
		const { tags } = this.state;
		this.setState({
			tags: tags.filter((tag, index) => index !== i)
		});
	}
	handleAddition(tag) {
		this.setState((state) => ({ tags: [ ...state.tags, tag ] }));
	}
	handleDrag(tag, currPos, newPos) {
		const tags = [ ...this.state.tags ];
		const newTags = tags.slice();

		newTags.splice(currPos, 1);
		newTags.splice(newPos, 0, tag);

		// re-render
		this.setState({ tags: newTags });
	}
	// END OF FUNCTIONS OF ` react-tag-input `

	// Change state on the FILE INPUTS, also validate them
	changeState(ev) {
		if (ev.target.files.length > 0) {
			let name = ev.target.name;
			let regExp = new RegExp('(.mp4|.mp3|.png|.jpg)', 'gi');
			if (regExp.test(ev.target.files[0].name)) {
				this.setState({
					[name]: 'green'
				});
				var file = ev.target.files[0];
				if (name == 'loadThumbnail') {
					let reader = new FileReader();
					reader.readAsDataURL(file);
					reader.onload = () => {
						this.setState({
							[name]: reader.result
						});
						console.log(reader);
					};
					reader.onerror = function(error) {
						console.log('Error: ', error);
					};
				} else {
					const data = new FormData();
					console.log('entra');
					data.append('file', file);
					data.append('filename', name);
					this.setState({
						[name]: data //data.get(name)
					});
				}
			} else
				this.setState({
					[name]: 'red'
				});
		}
	}

	// Upload the video function
	uploadVideo() {
		let video = {
			slug: agent.slugify(this.props.nameInput),
			name: this.props.nameInput,
			description: this.props.descriptionInput,
			video: this.state.loadFile,
			thumbnail: this.state.loadThumbnail,
			tags: [ 1 ]
		};
		this.props.uploadVideo(video);
	}

	// HTML
	render() {
		const { nameInput, fileInput, changeState, descriptionInput } = this.props;
		const { loadFile, loadThumbnail, tags, suggestions, delimiters } = this.state;
		return (
			<main className="main-upload-video">
				<form>
					<span>
						<label>Name : </label>
						<input
							placeholder="NAME OF THE VIDEO ..."
							value={nameInput}
							name="nameInput"
							onChange={(ev) => (ev.preventDefault(), changeState(ev.target.value, ev.target.name))}
						/>
					</span>
					<span>
						<label>Select Video : </label>
						<input type="file" onChange={this.changeState} name="loadFile" style={{ color: loadFile }} />
					</span>
					<span>
						<label>Description : </label>
						<textarea
							placeholder="DESCRIPTION OF THE VIDEO ..."
							value={descriptionInput}
							name="descriptionInput"
							onChange={(ev) => (ev.preventDefault(), changeState(ev.target.value, ev.target.name))}
						/>{' '}
					</span>
					<span>
						<label>Select Thumbnail : </label>
						<input
							type="file"
							onChange={this.changeState}
							name="loadThumbnail"
							accept="image/png, image/jpeg"
							style={{ color: loadThumbnail }}
						/>
					</span>
					<span>
						<ReactTags
							tags={tags}
							suggestions={suggestions}
							handleDelete={this.handleDelete}
							handleAddition={this.handleAddition}
							handleDrag={this.handleDrag}
							delimiters={delimiters}
						/>
					</span>
				</form>
				<button onClick={this.uploadVideo}>Upload</button>
			</main>
		);
	}
}

const mapDispatchToProps = (dispatch) => ({
	// Dispatch to change Single Value from the props
	changeState: (value, target) => {
		dispatch({ type: CHANGE_VALUE, value, target });
	},

	// Dispatch to prop the petition to the server for upload video
	uploadVideo: (video) => {
		dispatch({ type: VIDEO, payload: agent.Videos.uploadVideo(video) });
	}
});
const mapStateToProps = (state) => ({
	...state.videos
});

export default connect(mapStateToProps, mapDispatchToProps)(UploadVideos);
