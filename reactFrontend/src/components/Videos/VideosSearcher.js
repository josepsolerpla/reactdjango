import React from 'react';
import { connect } from 'react-redux';
import agent from '../../agent';

import VideosListing from './VideosListing';

import { VIDEOS, CHANGE_VALUE } from '../../constants/actionTypes';

const mapStateToProps = (state) => ({ ...state.videos });

const mapDispatchToProps = (dispatch) => ({
	onLoad: (page = 0, name) => dispatch({ type: VIDEOS, payload: agent.Videos.getAll(page, name), page }),
	changeValue: (target, value) => {
		dispatch({ type: CHANGE_VALUE, target, value });
		dispatch({
			type: VIDEOS,
			payload: agent.Videos.getAll(0, value),
			page: 0
		});
	}
});

class VideosSearcher extends React.Component {
	componentWillMount() {
		this.props.onLoad();
	}

	changePage = (e, { activePage }) => this.props.onLoad(activePage - 1);

	render() {
		const { videos, videosCount, currentPage, searchValue, changeValue } = this.props;
		return (
			<VideosListing
				videos={videos}
				videosCount={videosCount}
				changePage={this.changePage}
				currentPage={currentPage + 1}
				changeValue={changeValue}
				searchValue={searchValue}
			/>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(VideosSearcher);
