import React from 'react';
import settings from '../../settings';
import { Pagination, Popup } from 'semantic-ui-react';
import { Player } from 'video-react';
import { connect } from 'react-redux';

/**
 * This is just a render component all functions will come from the props
 * 
 * This compoentn is using semantic-ui-react components like Pagination or Popyp, is also using Player from video-react
 */
class VideosListing extends React.Component {
	// HTML
	render() {
		const {
			videos,
			videosCount,
			currentPage,
			changePage,
			searchValue,
			changeValue,
			errorServer,
			paginationNotShow,
			title
		} = this.props;
		if (errorServer) return <h1>ErrorServer</h1>;

		return (
			<div className="videoList__container">
				<div className="videoList__top">
					<div
						className="background"
						style={{
							backgroundImage: `url(${settings.BASE_URL}:8000/media/img/tiny.png)`
						}}
					/>
					<div className="content">
						<h1>{title ? title : `This is not Youtube`}</h1>
					</div>
				</div>
				<div className="videoList_search">
					<input
						value={searchValue}
						name="searchValue"
						onChange={(ev) => changeValue(ev.target.name, ev.target.value)}
						placeholder="Search ... "
					/>
					{paginationNotShow ? null : (
						<Pagination
							className="Pagination"
							totalPages={videosCount / 5}
							activePage={currentPage + 1}
							onPageChange={changePage}
						/>
					)}
				</div>
				<div className="videoList_items">
					{videos ? (
						videos.map((video) => (
							<div key={video.name} className="box">
								<Player
									playsInline
									poster={video.thumbnail}
									src={video.video}
									fluid={false}
									width={'100%'}
									height={272}
								/>
								<span className="description">
									<Popup
										trigger={
											<a href={`${settings.USE_PORT}/videos/${video.slug}`}>
												<h1>{video.name}</h1>
											</a>
										}
										children={
											<div
												style={{
													backgroundImage: `url(${video.thumbnail})`,
													backgroundSize: 'cover',
													minWidth: '350px',
													minHeight: '150px'
												}}
											>
												<h1>{video.name}</h1>
												<p>{video.description}</p>
											</div>
										}
									/>
								</span>
							</div>
						))
					) : (
						<p>Loading</p>
					)}
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	...state.videos
});
const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(VideosListing);
