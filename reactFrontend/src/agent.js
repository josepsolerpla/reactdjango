import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';
import settings from './settings';
// import { request } from 'https';
// import { request } from 'http';
// import { request } from 'https';

const superagent = superagentPromise(_superagent, global.Promise);

// const API_ROOT = 'https://conduit.productionready.io/api';
// const API_ROOT = 'http://yuse.ga:8000/api';
const API_ROOT = `${settings.BASE_URL}:8000/api`;

const encode = encodeURIComponent;
const responseBody = (res) => res.body;

let token = null;
const tokenPlugin = (req) => {
	if (token) {
		req.set('authorization', `Token ${token}`);
	}
};

const requests = {
	del: (url) => superagent.del(`${API_ROOT}${url}`).use(tokenPlugin).then(responseBody),
	get: (url) => superagent.get(`${API_ROOT}${url}`).use(tokenPlugin).then(responseBody),
	put: (url, body) => superagent.put(`${API_ROOT}${url}`, body).use(tokenPlugin).then(responseBody),
	post: (url, body) => superagent.post(`${API_ROOT}${url}`, body).use(tokenPlugin).then(responseBody)
};

const Auth = {
	current: () => requests.get('/user'),
	login: (email, password) => requests.post('/users/login', { user: { email, password } }),
	register: (username, email, password) => requests.post('/users', { user: { username, email, password } }),
	save: (user) => requests.put('/user', { user }),
	// Login validation / creation on sociallogin backend endpoint
	socialLogin: (username, email, password) =>
		requests.post('/users/sociallogin', { user: { username, email, password } })
};

const Tags = {
	getAll: () => requests.get('/tags')
};

const limit = (count, p) => `limit=${count}&offset=${p ? p * count : 0}`;
const omitSlug = (article) => Object.assign({}, article, { slug: undefined });
const Articles = {
	all: (page) => requests.get(`/articles?${limit(10, page)}`),
	byAuthor: (author, page) => requests.get(`/articles?author=${encode(author)}&${limit(5, page)}`),
	byTag: (tag, page) => requests.get(`/articles?tag=${encode(tag)}&${limit(10, page)}`),
	del: (slug) => requests.del(`/articles/${slug}`),
	favorite: (slug) => requests.post(`/articles/${slug}/favorite`),
	favoritedBy: (author, page) => requests.get(`/articles?favorited=${encode(author)}&${limit(5, page)}`),
	feed: () => requests.get('/articles/feed?limit=10&offset=0'),
	get: (slug) => requests.get(`/articles/${slug}`),
	unfavorite: (slug) => requests.del(`/articles/${slug}/favorite`),
	update: (article) => requests.put(`/articles/${article.slug}`, { article: omitSlug(article) }),
	create: (article) => requests.post('/articles', { article })
};

const Comments = {
	create: (slug, comment) => requests.post(`/articles/${slug}/comments`, { comment }),
	delete: (slug, commentId) => requests.del(`/articles/${slug}/comments/${commentId}`),
	forArticle: (slug) => requests.get(`/articles/${slug}/comments`)
};

const Profile = {
	follow: (username) => requests.post(`/profiles/${username}/follow`),
	get: (username) => requests.get(`/profiles/${username}`),
	unfollow: (username) => requests.del(`/profiles/${username}/follow`)
};

const Contact = {
	sendForm: (form) => requests.post(`/contact/`, { form })
};

//const Calendars = {
//	getAll: (page) => requests.get(`/calendar?${limit(5, page)}`),
//	getCalendar: (slug) => requests.get(`/calendar/${slug}`)
//};

const Videos = {
	// Get all the videos using pagination
	getAll: (page, name) => requests.get(`/videos?${limit(5, page)}${name ? `&name=${name}` : ''}`),
	// Get a single video
	get: (slug) => requests.get(`/videos/${slug}`),
	// Upload a video
	uploadVideo: (video) => requests.post('/videos', { video }),
	// Get my videos
	getMyVideos: (name) => requests.get(`/video/mylist${name ? `?&name=${name}` : ''}`)
};

// Function to slugify a text
function slugify(text) {
	return text
		.toString()
		.toLowerCase()
		.replace(/\s+/g, '-') // Replace spaces with -
		.replace(/[^\w\-]+/g, '') // Remove all non-word chars
		.replace(/\-\-+/g, '-') // Replace multiple - with single -
		.replace(/^-+/, '') // Trim - from start of text
		.replace(/-+$/, ''); // Trim - from end of text
}

export default {
	Articles,
	Auth,
	Comments,
	Profile,
	Tags,
	Contact,
	// Calendars,
	Videos,
	slugify,
	setToken: (_token) => {
		token = _token;
	}
};
