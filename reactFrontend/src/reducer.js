import article from './reducers/article';
import articleList from './reducers/articleList';
import auth from './reducers/auth';
import contact from './reducers/contact';
import { combineReducers } from 'redux';
import common from './reducers/common';
import editor from './reducers/editor';
import home from './reducers/home';
import profile from './reducers/profile';
import calendar from './reducers/calendar';
import videos from './reducers/videos';
import settings from './reducers/settings';
import { routerReducer } from 'react-router-redux';

export default combineReducers({
  article,
  articleList,
  auth,
  common,
  editor,
  home,
  profile,
  settings,
  contact,
  calendar,
  videos,
  router: routerReducer
});
