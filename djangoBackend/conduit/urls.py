from django.conf.urls import include, url
from django.contrib import admin

from django.conf import settings
from django.views.static import serve

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')), #'rest_framework.authentication.SessionAuthentication',

    url(r'^api/', include('conduit.apps.articles.urls', namespace='articles')),
    url(r'^api/', include('conduit.apps.videos.urls', namespace='videos')),
    url(r'^api/', include('conduit.apps.authentication.urls', namespace='authentication')),
    url(r'^api/', include('conduit.apps.profiles.urls', namespace='profiles')),

    url(r'^api/', include('conduit.apps.contact.urls', namespace='contact')),
]

if settings.DEBUG:
        urlpatterns += [
            url(r'^media/(?P<path>.*)$',serve, {
                'document_root': settings.MEDIA_ROOT
            }),
        ]