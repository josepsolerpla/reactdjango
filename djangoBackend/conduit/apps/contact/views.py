from django.core.mail import send_mail, BadHeaderError
from rest_framework import permissions, status, views, viewsets
from rest_framework.views import APIView
from django.http import HttpResponse, HttpResponseRedirect
from rest_framework.response import Response


class ContactView(APIView):

    def post(self, request, format=None):
        data = request.data
        email = data.get('email', None)
        name = data.get('name', None)
        message = data.get('message', None)

        if email is None:
            return Response({
                    'status': 'false',
                    'message': "'email' field is missing"
                }, status=status.HTTP_400_BAD_REQUEST)
        elif message is None:
            return Response({
                    'status': 'false',
                    'message': "'message' field is missing"
                }, status=status.HTTP_400_BAD_REQUEST)
            
        try:
            send_mail('Contact Form', message, 'josepsolerpla@gmail.com', [email])
        except BadHeaderError:
            return Response({
                    'status': 'false',
                    'message': 'BadHeaderError for your message'
                }, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        return Response({
                    'status': 'true',
                    'message': 'Success! Thank you for your message'
                }, status=status.HTTP_200_OK)
