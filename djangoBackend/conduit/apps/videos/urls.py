from django.conf.urls import include, url

from rest_framework.routers import DefaultRouter

from .views import (
    VideosViewSetAdmin, VideosViewSet,MyListAPIView
)

router = DefaultRouter(trailing_slash=False)
router.register(r'videos', VideosViewSet)

#Admin
router.register(r'videos_Admin', VideosViewSetAdmin)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^video/mylist/?$', MyListAPIView.as_view()),


]
