from conduit.apps.core.renderers import ConduitJSONRenderer


class VideosJSONRenderer(ConduitJSONRenderer):
    object_label = 'videos'
    pagination_object_label = 'videos'
    pagination_count_label = 'videosCount'
