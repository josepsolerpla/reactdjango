from django.db import models

from conduit.apps.core.models import TimestampedModel

class Videos(TimestampedModel):
    # Basic models fields
    name = models.CharField(db_index=True, max_length=255)
    slug = models.SlugField(db_index=True, max_length=255, unique=True)
    description = models.CharField(db_index=True, max_length=255,null=True)
    
    # Related field 1 by 1+
    author = models.ForeignKey(
        'profiles.Profile', on_delete=models.CASCADE, related_name='profile' , null=True
    )

    # File Fields, for this you should declare the path
    video = models.FileField(blank = True, null = True, upload_to="videos/")
    thumbnail = models.ImageField(default='img/default.png', upload_to='img', blank=True, null=True)

    # Related 1+ to 1+
    tags = models.ManyToManyField(
        'articles.Tag', related_name='videos'
    )

    def __str__(self):
        return self.name