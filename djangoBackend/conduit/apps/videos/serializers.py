from rest_framework import serializers

from conduit.apps.profiles.serializers import ProfileSerializer
from .relations import TagRelatedField

from .models import Videos

# Serializer to use bash64 images
class Base64ImageField(serializers.ImageField):

    def to_internal_value(self, data):
        from django.core.files.base import ContentFile
        import base64
        import six
        import uuid

        # Check if this is a base64 string
        if isinstance(data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid_image')

            # Generate file name:
            file_name = str(uuid.uuid4())[:12] # 12 characters are more than enough.
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension, )

            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension

# Videos Serializer
# Used for Create, List
class VideosSerializer(serializers.ModelSerializer):
    author = ProfileSerializer(required=False)
    tag = TagRelatedField(many=True, required=False, source='tags')
    
    # Fields for Files
    video = serializers.FileField(max_length=None)
    # I'm using my own converter, to convert bash54 to image file
    thumbnail = Base64ImageField(
        max_length=None, use_url=True,
    )

    class Meta:
        model = Videos
        # fields = [
        #     'author',
        #     'thumbnail',
        #     'tag',
        #     'video',
        #     'description',
        #     'slug'
        #     ]

        # We can set the dafult fields to __all__, the validation still works
        fields = '__all__'
