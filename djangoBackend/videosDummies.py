import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','conduit.settings')

import django
django.setup()

from conduit.apps.articles.models import Tag
from conduit.apps.videos.models import Videos
from conduit.apps.profiles.models import Profile
from conduit.apps.authentication.models import User
from faker import Faker

fake = Faker()

def call(N):
    for i in range(N):
        fake_tag = fake.word()
        fake_slug = fake.slug()
        tag = Tag.objects.get_or_create(tag=fake_tag, slug=fake_slug)[0]
        
        fake_slug = fake.slug()
        fake_name = fake.word()
        fake_description = fake.text()
        fake_video = "http://localhost:8000/media/videos/bulling.mp4"
        fake_thumbnail = "http://localhost:8000/media/img/Captura_de_pantalla_de_2018-11-12_19-31-04.png"
        author_josep = Profile.objects.get_or_create(user__username='josep')[0]
    
        Video = Videos.objects.get_or_create(slug=fake_slug, name=fake_name, description=fake_description, author=author_josep)[0]
        Video.tags.add(tag)
        


if __name__ == '__main__':
    print("Filling random data")
    call(10)
    print("Filling done ")
    